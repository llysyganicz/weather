﻿using MvvmCross.Converters;
using MvvmCross.Platforms.Ios.Core;
using Weather.Helpers;

namespace Weather.iOS
{
    public class Setup : MvxIosSetup<App>
    {
        public Setup()
        {
        }

        protected override void FillValueConverters(IMvxValueConverterRegistry registry)
        {
            base.FillValueConverters(registry);
            registry.AddOrOverwrite("BoolNegation", new BoolNegationConverter());
            registry.AddOrOverwrite("Temperature", new TemperatureConverter());
        }
    }
}
