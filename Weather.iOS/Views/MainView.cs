﻿using System;
using Cirrious.FluentLayouts.Touch;
using MvvmCross.Binding.BindingContext;
using MvvmCross.Platforms.Ios.Views;
using UIKit;
using Weather.ViewModels;

namespace Blank.Views
{
    public partial class MainView : MvxViewController<MainViewModel>
    {
        public MainView() : base("MainView", null)
        {
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var set = this.CreateBindingSet<MainView, MainViewModel>();

            set.Bind(CityText).To(vm => vm.City);

            set.Bind(SearchButton).To(vm => vm.GetWeatherData);
            set.Bind(SearchButton).For(b => b.Enabled).To(vm => vm.SearchIsEnabled);

            set.Bind(Panel).For(p => p.Hidden).To(vm => vm.DetailsVisible)
               .WithConversion("BoolNegation");
            set.Bind(CityValue).To(vm => vm.WeatherData.Name);
            set.Bind(TemperatureValue).To(vm => vm.WeatherData.Main.Temp)
               .WithConversion("Temperature");
            set.Bind(PressureValue).To(vm => vm.WeatherData.Main.Pressure);
            set.Bind(DescriptionValue).To(vm => vm.Description);

            set.Apply();

            //SetConstraints();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }

        private void SetConstraints()
        {
            View.SubviewsDoNotTranslateAutoresizingMaskIntoConstraints();

            var margin = 10;

            View.AddConstraints(CityText.AtLeftOf(View, margin),
                CityText.AtTopOfSafeArea(View, margin),
                CityText.ToLeftOf(SearchButton, margin),
                CityText.Above(Panel, margin));

            View.AddConstraints(SearchButton.ToRightOf(CityText, margin),
                SearchButton.AtTopOfSafeArea(View, margin),
                SearchButton.AtRightOf(View, margin),
                SearchButton.Above(Panel, margin));

            View.AddConstraints(Panel.AtLeftOf(View, margin),
                Panel.AtRightOf(View, margin),
                Panel.AtBottomOf(View, margin));

            Panel.AddConstraints(CityLabel.AtLeftOf(Panel),
                CityLabel.AtTopOf(Panel),
                CityLabel.ToLeftOf(CityValue, margin),
                CityLabel.WithRelativeWidth(CityValue, 1));

            //Panel.AddConstraints(CityValue.AtRightOf(Panel),
            //    CityValue.AtTopOf(Panel),
            //    CityValue.AtRightOf(CityLabel, margin),
            //    CityValue.WithRelativeWidth(CityLabel, 1));

            //Panel.AddConstraints(TemperatureLabel.AtLeftOf(Panel),
            //    TemperatureLabel.Below(CityLabel, margin),
            //    TemperatureLabel.ToLeftOf(TemperatureValue, margin),
            //    TemperatureLabel.WithRelativeWidth(TemperatureValue, 1));

            //Panel.AddConstraints(TemperatureValue.AtRightOf(Panel),
            //    TemperatureValue.Below(CityValue, margin),
            //    TemperatureValue.AtRightOf(TemperatureLabel, margin),
            //    TemperatureValue.WithRelativeWidth(TemperatureLabel, 1));

            //Panel.AddConstraints(PressureLabel.AtLeftOf(Panel),
            //    PressureLabel.Below(TemperatureLabel, margin),
            //    PressureLabel.ToLeftOf(PressureValue, margin),
            //    PressureLabel.WithRelativeWidth(PressureValue, 1));

            //Panel.AddConstraints(PressureValue.AtRightOf(Panel),
            //    PressureValue.Below(TemperatureValue, margin),
            //    PressureValue.AtRightOf(PressureLabel, margin),
            //    PressureValue.WithRelativeWidth(PressureLabel, 1));

            //Panel.AddConstraints(DescriptionLabel.AtLeftOf(Panel),
            //    DescriptionLabel.Below(PressureLabel, margin),
            //    DescriptionLabel.ToLeftOf(DescriptionValue, margin),
            //    DescriptionLabel.WithRelativeWidth(DescriptionValue, 1));

            //Panel.AddConstraints(DescriptionValue.AtRightOf(Panel),
                //DescriptionValue.Below(PressureValue, margin),
                //DescriptionValue.AtRightOf(DescriptionLabel, margin),
                //DescriptionValue.WithRelativeWidth(DescriptionLabel, 1));
        }
    }
}
