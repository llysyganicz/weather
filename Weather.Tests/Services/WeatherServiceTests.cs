﻿using FluentAssertions;
using Moq;
using Moq.Protected;
using NUnit.Framework;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Weather.Models;
using Weather.Services;

namespace Weather.Tests.Services
{
    [TestFixture]
    public class WeatherServiceTests
    {
        private const string Url = "https://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}&units=metric";

        private const string ValidResponse = @"{""coord"":{""lon"":-0.13,""lat"":51.51},""weather"":[{""id"":300,""main"":""Drizzle"",""description"":""light intensity drizzle"",""icon"":""09d""}],""base"":""stations"",""main"":{""temp"":7.17,""pressure"":1012,""humidity"":81,""temp_min""6,""temp_max"":8},""visibility"":10000,""wind"":{""speed"":4.1,""deg"":80},""clouds"":{""all"":90},""dt"":1485789600,""sys"":{""type"":1,""id"":5091,""message"":0.0103,""country"":""GB"",""sunrise"":1485762037,""sunset"":1485794875},""id"":2643743,""name"":""London"",""cod"":200}";

        [Test]
        public async Task GetWeatherDataAsync_WhenCalledWithValidCityParameter_ReturnsValidResult()
        {
            //Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Default);
            handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = HttpStatusCode.OK,
                   Content = new StringContent(ValidResponse),
               });

            var fullUrl = new Uri(string.Format(Url, "London", "APPID"));
            var service = new WeatherService(() => new HttpClient(handlerMock.Object) { BaseAddress = fullUrl });

            //Act
            var result = await service.GetWeatherDataAsync("London");

            //Assert
            result.Should().NotBeNull();
            result.Should().BeOfType(typeof(WeatherData));
            result.Coord.Lat.Should().Equals(51.51);
            result.Coord.Lon.Should().Equals(-0.13);
            result.Weather.Length.Should().Equals(1);
            result.Weather[0].Id.Should().Equals(300);
            result.Main.Temp.Should().Equals(7.17);
            result.Name.Should().Equals("London");
            result.Wind.Speed.Should().Equals(4.1);
        }

        [TestCase(HttpStatusCode.BadRequest)]
        [TestCase(HttpStatusCode.InternalServerError)]
        [TestCase(HttpStatusCode.Unauthorized)]
        public void GetWeatherDataAsync_WhenResponseCodeIsNot200_ThrowsException(HttpStatusCode statusCode)
        {
            //Arrange
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Default);
            handlerMock.Protected()
                .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage() { StatusCode = statusCode });

            var fullUrl = new Uri(string.Format(Url, "London", "APPID"));
            var service = new WeatherService(() => new HttpClient(handlerMock.Object) { BaseAddress = fullUrl });

            //Act
            Func<Task<WeatherData>> act = () => service.GetWeatherDataAsync("London");

            //Assert
            act.Should().Throw<Exception>().WithMessage("Request failed.");
        }
    }
}
