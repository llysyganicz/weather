﻿using MvvmCross.Converters;
using MvvmCross.Platforms.Android.Core;
using Weather.Helpers;

namespace Weather.Droid
{
    public class Setup : MvxAndroidSetup<App>
    {
        protected override void FillValueConverters(IMvxValueConverterRegistry registry)
        {
            base.FillValueConverters(registry);
            registry.AddOrOverwrite("Temperature", new TemperatureConverter());
        }
    }
}
