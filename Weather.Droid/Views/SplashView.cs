﻿using Android.App;
using Android.OS;
using MvvmCross.Platforms.Android.Presenters.Attributes;
using MvvmCross.Platforms.Android.Views;

namespace Weather.Droid.Views
{
    [MvxActivityPresentation]
    [Activity(Label = "Weather", MainLauncher = true, NoHistory = true, Theme = "@style/SplashTheme", Icon = "@drawable/splash")]
    public class SplashView : MvxSplashScreenActivity
    {
        public SplashView()
        : base(Resource.Layout.splash_screen)
        {
        }

        protected override void RunAppStart(Bundle bundle)
        {
            StartActivity(typeof(MainView));
            base.RunAppStart(bundle);
        }
    }
}
