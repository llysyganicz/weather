﻿using Android.App;
using Android.OS;
using MvvmCross.Platforms.Android.Views;
using Weather.ViewModels;

namespace Weather.Droid.Views
{
    [Activity(Label = "Weather")]
    public class MainView : MvxActivity<MainViewModel>
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.activity_main);
        }
    }
}
