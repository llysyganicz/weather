﻿using MvvmCross;
using MvvmCross.ViewModels;
using System.Net.Http;
using Weather.Helpers;
using Weather.Services;
using Weather.ViewModels;

namespace Weather
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            Mvx.IoCProvider.RegisterType<IWeatherService>(() => new WeatherService(() => new HttpClient()));
            RegisterAppStart<MainViewModel>();
        }
    }
}
