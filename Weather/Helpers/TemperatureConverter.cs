﻿using MvvmCross.Converters;
using System;
using System.Globalization;

namespace Weather.Helpers
{
    public class TemperatureConverter : IMvxValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (float)value;
            return string.Format("{0}°C", val);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
