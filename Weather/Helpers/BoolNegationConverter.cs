﻿using System;
using System.Globalization;
using MvvmCross.Converters;

namespace Weather.Helpers
{
    public class BoolNegationConverter : IMvxValueConverter
    {
        public BoolNegationConverter()
        {
        }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (bool)value;
            return !val;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var val = (bool)value;
            return !val;
        }
    }
}
