﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using Weather.Helpers;
using Weather.Models;

namespace Weather.Services
{
    public class WeatherService : IWeatherService
    {
        private const string Url = "https://api.openweathermap.org/data/2.5/weather?q={0}&APPID={1}&units=metric";
        private readonly Func<HttpClient> httpClientLocator;

        public WeatherService(Func<HttpClient> httpClientLocator)
        {
            this.httpClientLocator = httpClientLocator;
        }

        public async Task<WeatherData> GetWeatherDataAsync(string city)
        {
            var fullUrl = string.Format(Url, city, Secrets.OWMKey);

            using (var client = httpClientLocator())
            {
                var response = await client.GetAsync(fullUrl);

                if(response.StatusCode != System.Net.HttpStatusCode.OK)
                    throw new Exception("Request failed.");

                var stringContent = await response.Content.ReadAsStringAsync();
                var data = JsonConvert.DeserializeObject<WeatherData>(stringContent);
                return data;
            }
        }
    }
}
