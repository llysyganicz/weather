﻿using MvvmCross.Commands;
using MvvmCross.ViewModels;
using System.Threading.Tasks;
using Weather.Models;
using Weather.Services;

namespace Weather.ViewModels
{
    public class MainViewModel : MvxViewModel
    {
        private readonly IWeatherService weatherService;

        public MainViewModel(IWeatherService weatherService)
        {
            this.weatherService = weatherService;
            WeatherData = new WeatherData();
        }

        private string city;
        public string City
        {
            get => city;
            set
            {
                city = value;
                RaisePropertyChanged(() => City);
                RaisePropertyChanged(() => SearchIsEnabled);
                GetWeatherData.RaiseCanExecuteChanged();
            }
        }

        public bool SearchIsEnabled => !string.IsNullOrEmpty(City);


        private WeatherData weatherData;
        public WeatherData WeatherData
        {
            get => weatherData;
            set => SetProperty(ref weatherData, value);
        }

        public string Description => WeatherData.Weather.Length > 0 ? WeatherData.Weather[0].Description : string.Empty;

        private bool detailsVisible;
        public bool DetailsVisible
        {
            get => detailsVisible;
            set => SetProperty(ref detailsVisible, value);
        }

        public IMvxAsyncCommand GetWeatherData => new MvxAsyncCommand(GetWeatherDataExecute, () => SearchIsEnabled);

        private async Task GetWeatherDataExecute()
        {
            WeatherData = await weatherService.GetWeatherDataAsync(City);
            await RaisePropertyChanged(() => Description);
            DetailsVisible = true;
        }
    }
}
